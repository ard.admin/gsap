const path = require("path");

module.exports = () => {
    return {
        mode: "development",

        entry: path.resolve(__dirname, "index.js"),

        output: {
            path: path.resolve(__dirname, "dist"),
            filename: "testLib.js",
            library: "testLib",
            libraryTarget: "umd"
        },

        devtool: "source-map",

        module: {
            rules: [
                // BABEL
                {
                    test: /\.js$/,
                    loader: "babel-loader",
                    options: {
                        presets: [
                            ["env", {
                                "modules": false,
                                "debug": false,
                                "targets": {
                                    "browsers": ["last 2 versions", "> 2%"]
                                }
                            }]
                        ]
                    }
                }
            ]
        }
    };
};
