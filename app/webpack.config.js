const path = require("path");

module.exports = () => {
    return {
        mode: "development",

        entry: path.resolve(__dirname, "index.js"),

        output: {
            path: path.resolve(__dirname, "dist"),
            filename: "bundle.js"
        },

        devtool: "source-map",

        devServer: {
            contentBase: path.resolve(__dirname, "dist")
        },

        module: {
            rules: [
                // BABEL
                {
                    test: /\.js$/,
                    loader: "babel-loader",
                    options: {
                        presets: [
                            ["env", {
                                "modules": false,
                                "debug": false,
                                "targets": {
                                    "browsers": ["last 2 versions", "> 2%"]
                                }
                            }]
                        ]
                    }
                }
            ]
        }
    };
};
